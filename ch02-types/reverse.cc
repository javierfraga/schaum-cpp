#include <iostream>

int main(void)
{
    long m , d, n = 0;
    std::cout << "Enter a positive integer: " << std::endl;
    std::cin >> m;

    while ( m > 0 ) {
        d = m % 10; // d will be the right-most digit of m
        m /= 10; // then remove that digit from m
        n = 10 * n + d; // and append that digit to n
    }
    std::cout << "The reverse is " << n << std::endl;
    return 0;
}
