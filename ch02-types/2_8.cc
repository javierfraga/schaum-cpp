#include <iostream>
#include <iomanip>
using namespace std;


int main(void)
{
    std::cout << "Number of bytes used:\n" << std::endl;
    std::cout << setw(32) << "char: " << sizeof(char) << std::endl;
    std::cout << setw(32) << "short: " << sizeof(short) << std::endl;
    std::cout << setw(32) << "int: " << sizeof(int) << std::endl;
    std::cout << setw(32) << "long: " << sizeof(long) << std::endl;
    std::cout << setw(32) << "unsigned char: " << sizeof(unsigned char) << std::endl;
    std::cout << setw(32) << "unsigned short: " << sizeof(unsigned short) << std::endl;
    std::cout << setw(32) << "unsigned int: " << sizeof(unsigned int) << std::endl;
    std::cout << setw(32) << "unsigned long: " << sizeof(unsigned long) << std::endl;
    std::cout << setw(32) << "signed char: " << sizeof(signed char) << std::endl;
    std::cout << setw(32) << "float: " << sizeof(float) << std::endl;
    std::cout << setw(32) << "double: " << sizeof(double) << std::endl;
    std::cout << setw(32) << "long double: " << sizeof(long double) << std::endl;
    return 0;
}
