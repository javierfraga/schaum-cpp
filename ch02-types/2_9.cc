#include <iostream>
#include <iomanip>
#include <cfloat>
using namespace std;

int main(void)
{
    int fbits = 8 * sizeof (float);
    std::cout << "float uses " << fbits << " bits:" << std::endl;
    std::cout << setw(16) << FLT_MANT_DIG -1 << " bits for the mantissa," << std::endl;
    std::cout << setw(16) << fbits - FLT_MANT_DIG << " bits for its exponent," << std::endl;
    std::cout << setw(16) << 1 << " bits for its sign," << std::endl;
    std::cout << setw(16) << "" << " to obtain:" << FLT_DIG << " sig. digits"<< std::endl;
    std::cout <<  std::endl;

    /*
     *-----------------------------------------------
     */

    int dbits = 8 * sizeof (double);
    std::cout << "double uses " << dbits << " bits:" << std::endl;
    std::cout << setw(16) << DBL_MANT_DIG -1 << " bits for the mantissa," << std::endl;
    std::cout << setw(16) << dbits - DBL_MANT_DIG << " bits for its exponent," << std::endl;
    std::cout << setw(16) << 1 << " bits for its sign," << std::endl;
    std::cout << setw(16) << "" << " to obtain:" << DBL_DIG << " sig. digits"<< std::endl;
    std::cout <<  std::endl;

    /*
     *-----------------------------------------------
     */

    int ddbits = 8 * sizeof (long double);
    std::cout << "long double uses " << ddbits << " bits:" << std::endl;
    std::cout << setw(16) << LDBL_MANT_DIG -1 << " bits for the mantissa," << std::endl;
    std::cout << setw(16) << ddbits - LDBL_MANT_DIG << " bits for its exponent," << std::endl;
    std::cout << setw(16) << 1 << " bits for its sign," << std::endl;
    std::cout << setw(16) << "" << " to obtain:" << LDBL_DIG << " sig. digits"<< std::endl;
    return 0;
}
