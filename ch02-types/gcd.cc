#include <iostream>

int main(void)
{
    int m , n , r;
    std::cout << "Enter two positive integers: " << std::endl;
    std::cin >> m >> n;
    if ( m < n ) {
        int temp = m;
        m = n;
        n = temp;
    }
    std::cout << "The GCD of " << m << " and " << n << " is: ";
    while ( n > 0 ) {
        r = m % n;
        m = n;
        n = r;
    }
    std::cout << m << std::endl;
    return 0;
}
