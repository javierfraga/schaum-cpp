#include <iostream>

int main(void)
{
    /*
     *Enum just defines new unsigned integer type
     */
    std::cout << "Enum just defines new unsigned integer type" << std::endl;
    std::cout << std::endl;
    enum Day { MON , TUES , WED , THU , FRI , SAT , SUN }; 
    float high[ SUN + 1 ] = { 88.3 , 95.0 , 91.2 , 89.9 , 91.4 , 92.5 , 86.7 };

    for (int day = 0; day < SUN; ++day) {
        std::cout << "The high temperature for day " << day << " was " << high[day] << std::endl;
    }

    return 0;
}
