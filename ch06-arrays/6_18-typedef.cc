#include <iostream>
#include <algorithm>
using namespace std;

/*
 *Typedef is just a synonym for an existing type, in this case a float
 */
typedef float Sequence[];

void print( Sequence a , unsigned int count )
{
    for (unsigned int i = 0; i < count; ++i) {
        std::cout << a[i] << ", ";
    }
    std::cout << std::endl;
}
void sort( Sequence a , int n )
{
    for (int i = n - 1; i > 0; i--) {
        for (int j = 0; j < i; ++j) {
            if ( a[j] > a[j+1] ) {
                swap( a[j] , a[j+1] );
            }
        }
    }
}

int main(void)
{
    std::cout << "Typedef is just a synonym for an existing type, in this case a float" << std::endl;
    std::cout << std::endl;
    Sequence a = { 55.5 , 22.5 , 99.9 , 66.6 , 44.4 , 88.8 , 33.3 , 77.7 };
    int size = sizeof(a) / sizeof(float);
    print( a , size );
    sort( a , size );
    print( a , size );
    
    return 0;
}
