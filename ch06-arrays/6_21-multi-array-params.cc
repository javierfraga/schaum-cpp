#include <iostream>

/*
 *When multi-dimensional array is passed to function 
 *all dimensions except the first must be specified
 *so complier can compute location of each element in array
 */
int numZeros( int a[][4][3] , int n1 , int n2 , int n3 )
{
    int count = 0;
    for (int i = 0; i < n1; ++i) {
        for (int j = 0; j < n2; ++j) {
            for (int k = 0; k < n3; ++k) {
                if ( a[i][j][k] == 0 ) {
                    ++count;
                }
            }
        }
    }
    
    return count;
}

int main(void)
{
    std::cout << "*When multi-dimensional array is passed to function all dimensions except the first must be specified so complier can compute location of each element in array " << std::endl;
    std::cout << std::endl;
    int a[2][4][3] = {
        {
            { 5 , 0 , 2 } , { 0 , 0 , 9 } , { 4 , 1 , 0 } , { 7 , 7 , 7 }
        } , 
        {
            { 3 , 0 , 0 } , { 8 , 5 , 0 } , { 0 , 0 , 0 } , { 2 , 0 , 9 }
        }
    };
    std::cout << "This array has " << numZeros( a , 2 , 4 , 3 ) << " zeros." << std::endl;
    
    return 0;
}
