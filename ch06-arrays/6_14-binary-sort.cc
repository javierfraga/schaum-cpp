#include <iostream>

int index( int x , int a[] , int n )
{
    /*
     *Binary Search
     */
    int lo = 0 , hi = n -1 , i;
    while ( lo <= hi ) {
        i = (lo + hi)/2;
        if ( a[i] == x ) {
            return i; // found it
        }
        if ( a[i] < x ) {
            lo = i + 1; // continue search upper half
        } else {
            hi = i - 1; // continue search lower half
        }
    }

    return n; // x was not found in array
}

int main(void)
{
    int a[] = { 22 , 33 , 44 , 55 , 66 , 77 , 88 };
    int size = sizeof(a)/sizeof(int);
    std::cout << "index(44,a,size) = " << index(44,a,size) << std::endl;
    std::cout << "index(60,a,size) = " << index(60,a,size) << std::endl;
    
    return 0;
}
