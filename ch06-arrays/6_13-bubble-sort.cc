#include <iostream>
#include <algorithm>
using namespace std;

void print( float a[8] , unsigned int count )
{
    for (unsigned int i = 0; i < count; ++i) {
        std::cout << a[i] << ", ";
    }
    std::cout << std::endl;
}
void sort( float a[] , int n )
{
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n-1; ++j) {
            if ( a[j] > a[j+1] ) {
                swap( a[j] , a[j+1] );
            }
        }
    }
}

int main(void)
{
    float a[] = {55.5 , 22.5 , 99.9, 66.6, 44.4 , 88.8 , 33.3 , 77.7};
    int count = sizeof(a)/sizeof(float);
    print( a , count);
    sort( a , count );
    print( a , count);
    return 0;
}
