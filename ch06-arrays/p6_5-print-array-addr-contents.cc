#include <iostream>

int main(void)
{
    unsigned int a[] = { 22 , 44 , 66 , 88 };
    std::cout << "a   = " << a   << ", *a     = " << *a     << ", a[0] = " << a[0] << std::endl;
    std::cout << "a+1 = " << a+1 << ", *(a+1) = " << *(a+1) << ", a[1] = " << a[1] << std::endl;
    std::cout << "a+2 = " << a+2 << ", *(a+2) = " << *(a+2) << ", a[2] = " << a[2] << std::endl;
    std::cout << "a+3 = " << a+3 << ", *(a+3) = " << *(a+3) << ", a[3] = " << a[3] << std::endl;
    
    return 0;
}
