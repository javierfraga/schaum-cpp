#include <iostream>

int main(void)
{
    std::cout << "\\a:" << "\a" << std::endl;
    std::cout << "\\b:" << "\b" << std::endl;
    std::cout << "\\n:" << "\n" << std::endl;
    std::cout << "\\r:" << "\r" << std::endl;
    std::cout << "\\t:" << "\t" << std::endl;
    std::cout << "\\v:" << "\v" << std::endl;
    std::cout << "\\':" << "\'" << std::endl;
    std::cout << "\\\":" << "\"" << std::endl;
    std::cout << "\\?:" << "\?" << std::endl;
    return 0;
}
