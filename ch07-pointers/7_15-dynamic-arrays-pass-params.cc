#include <iostream>

void getAddr( double *&a , int &n )
{
    std::cout << "__info()__" << std::endl;
    std::cout << "&a    :  " << &a << std::endl;
    std::cout << "&a[0] :  " << &a[0] << std::endl;
    std::cout << "a     :  " << a << std::endl;
    std::cout << "&n    :  " << &n << std::endl;
    std::cout << "n     :  " << n << std::endl;
}

void get( double *&a , int &n )
{
    std::cout << "--------------------" << std::endl;
    std::cout << "Enter number of items: ";
    std::cin >> n;

    std::cout << "pre new=>";
    getAddr( a , n );
    a = new double[n];
    std::cout << "post new=>";
    getAddr( a , n );
    std::cout << "Enter " << n << " items, one per line:" << std::endl;
    std::cout << "\t__get()__" << std::endl;
    for (int i = 0; i < n; ++i) {
        std::cout << "\t" << i+1 << ": " << &a[i] << " ";
        std::cin >> a[i];
    }
}

void print( double *a , int n )
{
    std::cout << "\t__print()__" << std::endl;
    for (int i = 0; i < n; ++i) {
        std::cout << "\t" << i+1 << ": " << &a[i] << " " << a[i] << std::endl;
    }
}

int main(void)
{
    double *a;      // a is simply an unallocated pointer
    int n;
    std::cout << "&a    :  " << &a << std::endl;
    std::cout << "&a[0] :  " << &a[0] << std::endl;
    std::cout << "a     :  " << a << std::endl;
    std::cout << "&n    :  " << &n << std::endl;
    std::cout << "n     :  " << n << std::endl;
    std::cout << "--------------------" << std::endl;
    getAddr( a , n );
    get( a , n );   // now a is an array of n doubles
    getAddr( a , n );
    print( a , n );
    delete []a;     // now a is simply an unallocated pointer again
    get( a , n );   // now a is an array of n doubles
    getAddr( a , n );
    print( a , n );
    delete []a;     // now a is simply an unallocated pointer again
    
    return 0;
}
