#include <iostream>

int main(void)
{
    std::cout << "int *pn = &n" << std::endl;
    std::cout << "*pn is the dereferencing operator" << std::endl;
    std::cout << std::endl;
    int n = 44;
    std::cout << "n = " << n << ", &n = " << &n << std::endl;
    int *pn = &n;
    std::cout << "*pn = " << *pn << ", pn = " << pn << ", &pn = " << &pn << std::endl;
    
    return 0;
}
