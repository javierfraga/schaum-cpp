#include <iostream>

int main(void)
{
    std::cout << "Shows when pointer is in arthimetic op its base value (increment value) is its sizeof" << std::endl;
    std::cout <<  std::endl;

    const int SIZE = 3;
    short a[SIZE] = { 22 , 33 , 44 };

    std::cout << "a = " << a << std::endl;
    std::cout << "sizeof(short) = " << sizeof(short) << std::endl;
    std::cout << "Since p is a short it increments by 2 with p++" << std::endl;
    short *end = a + SIZE;
    short sum = 0;
    for (short *p = a; p < end; p++) {
        sum += *p;
        std::cout << "\t p = " << p;
        std::cout << "\t *p = " << *p;
        std::cout << "\t sum = " << sum << std::endl;
    }
    std::cout << "end = " << end << std::endl;
    
    return 0;
} 
