#include <iostream>

int main(void)
{

    std::cout << "int n = 44;     " << std::endl;
    std::cout << "int *pn = &n; // pn holds the address of n" << std::endl;
    std::cout << "int **ppn = &pn; // ppn holds the address of pn" << std::endl;
    std::cout << "int nn = *pn; // nn is a duplicate of n    " << std::endl;
    std::cout << "int &rpn = *pn; // rpn is a reference for n" << std::endl;
    std::cout << "int &rn = n; // rn is a reference for n" << std::endl;
    std::cout << std::endl;

    int n = 44;
    int *pn = &n; // pn holds the address of n
    int **ppn = &pn; // ppn holds the address of pn
    int nn = *pn; // nn is a duplicate of n
    int &rpn = *pn; // rpn is a reference for n
    int &rn = n; // rpn is a reference for n

    std::cout << "n = " << n << "         , *pn = " << *pn << "        , **ppn = " << **ppn << "        , nn = " << nn << "        , rpn = " << rpn << "        , rn = " << rn << std::endl;
    std::cout << "&n = " << &n << ", pn = " << pn << ", *ppn = " << *ppn << ", &nn = " << &nn << ", &rpn = " << &rpn << ", &rn = " << &rn << std::endl;
    std::cout << "&n = " << &n << ", &pn = " << &pn << ", ppn = " << ppn << ", &nn = " << &nn << ", &rpn = " << &rpn <<  ", &rn = " << &rn << std::endl;
    std::cout << "&n = " << &n << ", &pn = " << &pn << ", &ppn = " << &ppn << ", &nn = " << &nn << ", &rpn = " << &rpn <<  ", &rn = " << &rn << std::endl;
    
    return 0;
}
