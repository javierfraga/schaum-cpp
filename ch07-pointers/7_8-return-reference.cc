#include <iostream>
//#include <stdio.h>

int &max( int &m , int &n )
{
    return ( m > n ? m : n );
}

//int main(int argc, char *argv[])
int main(void)
{

    std::cout << "Tested this also works in C, not sure why I was so confused before" << std::endl;
    std::cout << std::endl;

    int m = 44 , n = 22;

    std::cout << "m:" << m << ", n:" << n << ", max(m,n):" << max( m , n ) << std::endl;
    max( m , n ) = 55;
    std::cout << "m:" << m << ", n:" << n << ", max(m,n):" << max( m , n ) << std::endl;

    //printf("m:%d , n:%d , max(m,n):%d\n", m , n , max(m,n));
    //max( m , n ) = 55;
    //printf("m:%d , n:%d , max(m,n):%d\n", m , n , max(m,n));
    
    return 0;
}
