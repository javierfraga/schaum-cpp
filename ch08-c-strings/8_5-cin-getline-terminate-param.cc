#include <iostream>

int main(void)
{
    std::cout << "Cin.getline will terminate on ',' in this example" << std::endl;
    std::cout << std::endl;
    char clause[80];
    do {
        std::cin.getline( clause , 80 , ',' );
        if ( *clause ) {
            std::cout << "\t[" << clause << "]" << std::endl;
        }
    } while ( *clause );
    
    return 0;
}
