#include <iostream>
#include <cstring>

int main(void)
{
    std::cout << "char s[] = \"ABCD\", automatically appends null to end" << std::endl;
    std::cout << std::endl;
    char s[] = "ABCD";

    for (unsigned int i = 0; i < (strlen(s) + 1); ++i) {
        std::cout << "s[" << i << "] = '" << s[i] << "'\n" << std::endl;
    }
    
    return 0;
}
