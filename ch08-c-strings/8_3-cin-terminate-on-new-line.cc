#include <iostream>

int main(void)
{
    std::cout << "cin terminates on any whitespace but only flushes on ENTER" << std::endl;
    std::cout << std::endl;
    char word[80];

    do {
        /*
         * Cin terminates on any whitespace 
         * But only flushes on ENTER
         * CTRL-Z send EOF to cin
         * EOF==null
         */
        std::cin >> word;
        if ( *word ) {
            std::cout << "\t\"" << word << "\"\n" << std::endl;
        }
    } while ( *word );
    
    return 0;
}
