#include <iostream>

/*
 *Doesn't compile with -Wall
 */
//int nextIntPeek()
//{
    //char ch;
    //int n ;
    //// Doesn't compile
    //while ( ch = std::cin.peek() ) {
        //if ( ch >= '0' && ch <= '9' ) {
            //// puts back so cin below can get full number, terminates on whitespace
            //std::cin >> n;
            //break;
        //} else {
            //std::cin.get(ch);
        //}
    //}

    //return n;
//}

int nextInt()
{
    char ch;
    int n ;
    while ( std::cin.get(ch) ) {
        if ( ch >= '0' && ch <= '9' ) {
            // puts back so cin below can get full number, terminates on whitespace
            std::cin.putback(ch); 
            std::cin >> n;
            break;
        }
    }

    return n;
}

int main(void)
{
    std::cout << "Ex: 'What is 305 plus 9416?'" << std::endl;
    std::cout << std::endl;
    int m = nextInt() , n = nextInt();
    //int m = nextIntPeek() , n = nextIntPeek();
    // Ignore max 80 chars until '\n' is encountered
    std::cin.ignore( 80 , '\n' );
    std::cout << m << " + " << n << " = " << m+n << std::endl;
    
    return 0;
}
