#include <iostream>

int main(void)
{
    char line[80];
    do {
        std::cin.getline( line , 80 );
        if ( *line ) {
            std::cout << "\t[" << line << "]" << std::endl;
        }
    } while ( *line );
    
    return 0;
}
