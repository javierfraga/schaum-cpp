#include <iostream>
#include <iomanip>
using namespace std;

/*
 *Just finds numnber of combinations
 */
long comb( int n , int k )
{
    if ( n < 0 || k < 0 || k > n ) {
        return 0;
    }
    
    long c = 1;
    for (int i = 1; i <= k; i++ , n--) {
        c = c*n/i;
    }
    return c;
}

/*
 *Formats output to triangle
 */
int main(void)
{
    const int m = 13;

    for (int i = 0; i < m; ++i) {
        for (int j = 1; j < m-i; ++j) {
            /*
             *Only need to make whitespace of leftside
             */
            std::cout << setw(2) << " ";
        }
        for (int j = 0; j <= i; ++j) {
            /*
             *Now just make enough room for your number
             */
            std::cout << setw(4) << comb( i , j );
        }
        std::cout <<  std::endl;
    }
    return 0;
}
