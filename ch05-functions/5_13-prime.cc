#include <iostream>
#include <cmath>
using namespace std;

bool isPrime( int n )
{
    float sqrtn = sqrt( n );
    if ( n < 2 ) { // 0 and 1 are not prime
        return false;
    }
    if ( n < 4 ) { // 2 and 3 are the first primes
        return true;
    }
    if ( n % 2 == 0 ) { // 2 is the only even prime
        return false;
    }
    for (int d = 3; d < sqrtn; d +=2) {
        if ( n%d == 0 ) {
            return false; // n has a divisor
        }
    }
    return true; // n has no divisor all conditions passsed
}

int main(void)
{
    for (int i = 0; i < 80; ++i) {
        if ( isPrime(i) ) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
    return 0;
}
