#include <iostream>

int isSquare( int n  )
{
    int i = 0;
    /*
     *Counts sqaures immediately before number
     */
    while ( i*i < n ) {
        ++i;
    }

    /*
     *Tests if the number is squared
     */
    if ( i*i == n ) {
        return true;
    } else {
        return false;
    }
}

int main(void)
{
    const int MAX = 20;
    for (int i = 0; i < MAX; ++i) {
        if ( isSquare(i) ) {
            std::cout << i << " is square." << std::endl;
        } else {
            std::cout << i << " is not square." << std::endl;
        }
    }
    return 0;
}
