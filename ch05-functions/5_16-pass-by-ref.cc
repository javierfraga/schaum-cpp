#include <iostream>

void swap( float &x , float &y )
{
    float temp = x;
    x = y;
    y = temp;
}

int main(void)
{
    float a = 22.2, b = 44.4;

    std::cout << "So much easier to do in c++:" << std::endl;
    std::cout << "a = " << a << " , b = " << b << std::endl;
    swap( a , b );
    std::cout << "a = " << a << " , b = " << b << std::endl;
    return 0;
}
