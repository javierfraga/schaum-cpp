#include <iostream>

inline int cube( int x )
{
    return x*x*x;
}

int main(void)
{
    // test the cube() function:
    std::cout << "This is an inline function result:" << cube(4) << std::endl;
    std::cout <<  std::endl;

    std::cout << "inline benefits: don't have to spend time:" << std::endl;
    std::cout << "1. invoking function" << std::endl;
    std::cout << "2. pass params" << std::endl;
    std::cout << "3. allocate storage for locals" << std::endl;
    std::cout << "4. store current vars" << std::endl;
    std::cout << "5. store location of main" << std::endl;
    std::cout <<  std::endl;
    
    std::cout << "inline negatives:" << std::endl;
    std::cout << "1. inline large functions many times grows binary" << std::endl;
    std::cout << "2. limits portability" << std::endl;

    return 0;
}
