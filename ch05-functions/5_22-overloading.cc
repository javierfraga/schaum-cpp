#include <iostream>

int max( int x , int y )
{
    return ( x > y ? x : y );
}

int max( int x , int y , int z )
{
    int m = ( x > y ? x : y );
    return ( z > m ? z : m );
}

int main(void)
{
    std::cout << "max( 99 , 77 ):" << max( 99 , 77 ) << "\t" << "max( 55 , 66 , 33 ):" << max( 55 , 66 , 33 ) << std::endl;
    return 0;
}
