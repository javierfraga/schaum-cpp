#include <iostream>
using namespace std;

int getDigit( long n , int k )
{
    for (int i = 0; i < k; ++i) {
        n /= 10; // removes right-most digit to until reaches target
    }
    return n%10; // %10 always gets last digit
}

int  getDigitCount( int n )
{
    int count = 0;
    while ( n ) {
        n /= 10;
        count++;
    }

    return count;
}

int main(void)
{
    int n , k;
    std::cout << "Integer: ";
    std::cin >> n;
    while (true){
        do {
            std::cout << "Enter digit less the integer count (zero index): ";
            std::cin >> k;
        } while ( (getDigitCount(n) - 1) < k );
        
        std::cout << "Digit number " << k << " of " << n << " is " << getDigit(n,k) << std::endl;
        std::cout << std::endl;
    };

    return 0;
}
