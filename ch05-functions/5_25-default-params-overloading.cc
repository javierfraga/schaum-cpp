#include <iostream>

double p( double x , double a0 , double a1 = 0 , double a2 = 0 , double a3 = 0 )
{
    return a0 + ( a1 + (a2 + a3*x)*x ) * x;
}

int main(void)
{
    double x = 2.0003;

    std::cout << "p(x,7) " << p(x,7) << std::endl;
    std::cout << "p(x,7,6) " << p(x,7,6) << std::endl;
    std::cout << "p(x,7,6,5) " << p(x,7,6,5) << std::endl;
    std::cout << "p(x,7,6,5,4) " << p(x,7,6,5,4) << std::endl;

    return 0;
}
