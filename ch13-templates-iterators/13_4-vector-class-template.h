#ifndef VECTOR
#define VECTOR

template<class T>
class Vector
{
protected:
    T* data;
    unsigned size;
    void copy( const Vector<T>& );

public:
    Vector( unsigned n=8 ) : size(n) , data( new T[size] ) {}
    Vector( const Vector<T>& v ) : size(v.size) , data(new T[size]){ copy(v); }
    virtual ~Vector() { delete [] data; }
    Vector<T>& operator=( const Vector<T>& );
    T& operator[]( unsigned i ) const { return data[i]; }
    unsigned getSize() { return size; }
};

template<class T>
Vector<T>& Vector<T>::operator=( const Vector<T>& v ) {
    size = v.size;
    data= new T[size];
    copy(v);
    return *this;
}

template<class T>
void Vector<T>::copy( const Vector<T>& v ) {
    unsigned min_size = ( size < v.size ? size : v.size );
    for (unsigned i = 0; i < min_size; ++i) {
        data[i] = v.data[i];
    }
}

#endif /* VECTOR */
