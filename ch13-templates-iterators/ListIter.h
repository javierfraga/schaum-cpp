#ifndef LISTITER_H
#define LISTITER_H

#include <iostream>
#include "List.h"
#include "Iterator.h"
#include "ListNode.h"

template<class T>
class ListIter  :   public Iterator<T>
{
protected:
    ListNode<T>* current;  // points to current node
    ListNode<T>* previous; // points to previous node
    List<T>& list;         // this is the list being transversed
    

public:
    ListIter( List<T>& l ): list(l) { reset(); }
    virtual ~ListIter();
    virtual void reset() { previous = NULL ; current = list.first; }
    virtual T operator()(){ return current->data; }
    virtual void operator=( T t ) { current->data = t; }
    virtual int operator!();  // Determine whether current exists
    virtual int operator++(); // Advance iterator to next item
    void insert( T t );       // Insert t after current item
    void preInsert( T t );    // Insert t before current item
    void remove();            // Remove current item
};


template<class T>
int ListIter<T>::operator!() {
    if ( current == NULL ) {
        if ( previous == NULL ) {
            current = list.first;
        } else {
            current = previous->next;
        }
    }
    return ( current != NULL );
}

template<class T>
int ListIter<T>::operator++() {
   if ( current == NULL ) {
       if ( previous == NULL ) {
           current = list.first;
       } else {
           current = previous->next;
       }
   } else {
       previous = current;
       current = current->next;
   } 

   return ( current !=NULL );
}

template<class T>
void ListIter<T>::insert(T t) {
    ListNode<T>* p = list.newNode( t, 0 );
    if ( list.isEmpty() ) {
        list.first = p;
    } else {
        p->next = current->next;
        current->next = p;
    }
}

template<class T>
void ListIter<T>::preInsert(T t) {
    ListNode<T>* p = list.newNode( t , current );
    if ( current == list.first ) {
        list.first = previous = p;
    } else {
        previous->next = p;
    }
}

template<class T>
void ListIter<T>::remove() {
    if ( current == list.first ) {
        list.first = current->next;
        delete current;
        current = 0;
    }
}
#endif /* LISTITER_H */
