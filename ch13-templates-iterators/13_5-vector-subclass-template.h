#ifndef ARRAY
#define ARRAY

#include "13_4-vector-class-template.h"

template<class T>
class Array : public Vector<T>
{
protected:
    int i0;

public:
    Array( int i , int j ) : i0(i) , Vector<T>(j+1) {}
    Array( const Array<T>& v ) : i0( v.i0 ) , Vector<T>(v) {}
    T& operator[]( int i ) const { return Vector<T>::operator[](i-i0); }
    int firstSubscript() const { return i0; }
    int lastSubscript() const { return i0+size-1; }
    virtual ~Array();
};

#endif /* ARRAY */
