#include <iostream>

template<class T>
class Stack
{
private:
    int size;
    int top;
    T* data;

public:
    Stack( int s = 100 ) : size(s) , top(-1) { data = new T[size]; }
    virtual ~Stack() { delete [] data; }
    void push( const T& x ) { data[++top] = x; }
    T pop() { return data[top--]; }
    int isEmpty() const { return top == -1; }
    int isFull() const { return top == size -1; }
};

int main(void)
{
    Stack<int> intStack1(5);
    Stack<int> intStack2(10);
    Stack<char> charStack(8);

    intStack1.push(77);
    charStack.push('A');
    intStack2.push(22);
    charStack.push('E');
    charStack.push('K');
    intStack2.push(44);

    std::cout << intStack2.pop() << std::endl;
    std::cout << intStack2.pop() << std::endl;

    if ( intStack2.isEmpty() ) {
        std::cout << "intStack2 is empty" << std::endl;
    }
    
    return 0;
}
