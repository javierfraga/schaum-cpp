#include <iostream>
#include "13_4-vector-class-template.h"

int main(void)
{
    Vector<short> v;
    v[5] = 127;
    Vector<short> w = v , x(3);
    std::cout << w.getSize() << std::endl;
    
    return 0;
}
