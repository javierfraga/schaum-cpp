#ifndef LISTNODE_H
#define LISTNODE_H

#include "List.h"
#include "ListIter.h"


template<class T>
class ListNode
{
    friend class List<T>;
    friend class ListIter<T>;
protected:
    T data;         // data field
    ListNode* next; // points to next node in list

public:
    ListNode( T& t , ListNode<T>* p )   :   data(t) , next(p) {}
    virtual ~ListNode();
};

#endif /* LISTNODE_H */
