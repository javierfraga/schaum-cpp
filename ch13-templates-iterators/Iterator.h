#ifndef ITERATOR_H
#define ITERATOR_H

template<class T>
class Iterator
{
public:
    virtual void reset() =0;          // Initialize the iterator
    virtual T operator()() =0;        // Read current value
    virtual void operator=( T t ) =0; // Write current value
    virtual int operator!() =0;       // Determine whether items exists
    virtual int operator++() =0;      // Advance to next list

};

#endif /* ITERATOR_H */
