#ifndef LIST_H
#define LIST_H

#include "ListIter.h"
#include "ListNode.h"

template<class T>
class List
{
    friend class ListIter<T>;
protected:
    ListNode<T>* first;
    ListNode<T>* newNode( T& t , ListNode<T>* p ){
        ListNode<T>* q = new ListNode<T>( t, p );
        return q;
    }

public:
    List()  :   first(0) {};
    virtual ~List();
    void insert( T t ); // Insert t at front of list
    int remove( T& t ); // Remove first item t in list
    bool isEmpty() { return (first ==0); }
    void print();
};

#endif /* LIST_H */
