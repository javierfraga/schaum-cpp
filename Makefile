# This defines our compiler and linker, as we've seen before.
CXX = g++
LD = g++ 

# These are the options we pass to the compiler. 
# -std=c++1y means we want to use the C++14 standard (called 1y in this version of Clang). 
# -stdlib=libc++ specifies that we want to use the standard library implementation called libc++
# -c specifies making an object file, as you saw before
# -g specifies that we want to include "debugging symbols" which allows us to use a debugging program.
# -O0 specifies to do no optimizations on our code.
# -Wall, -Wextra, and -pedantic tells the compiler to look out for common problems with our code. -Werror makes it so that these warnings stop compilation.
#CXXFLAGS = -std=c++1y -stdlib=libc++ -c -g -O0 -Wall -Wextra -Werror -pedantic -ggdb
#This needed to change to make ch13-templates-iterators code compile, errors
CXXFLAGS = -std=c++1y -g -O0 -Wall -Wextra -pedantic -ggdb
#CXXFLAGS = -std=c++1y -g -O0 -Wall -Wextra -Werror -pedantic -ggdb

# These are the options we pass to the linker.
# The first two are the same as the compiler flags.
# -l<something> tells the linker to go look in the system for pre-installed object files to link with.
# Here we want to link with the object files from libpng (since we use it in our code) and libc++. Remember libc++ is the standard library implementation. 
#LDFLAGS = -std=c++1y -stdlib=libc++ -lpng -lc++abi -lpthread
LDFLAGS = -std=c++1y -lpng -lpthread
#LDFLAGS = -std=c++1y 

all:
	make ch01-basics/one ch01-basics/two ch01-basics/three ch01-basics/four ch01-basics/five
	make ./ch02-types/2_8 ./ch02-types/2_9 ./ch02-types/gcd ./ch02-types/reverse
	make ./ch05-functions/5_13-prime ./ch05-functions/5_16-pass-by-ref ./ch05-functions/5_20-inline ./ch05-functions/5_22-overloading ./ch05-functions/5_25-default-params-overloading ./ch05-functions/p5_16-format-output ./ch05-functions/p5_17-extract-any-digit ./ch05-functions/p5_22-is-squared
	make ./ch06-arrays/6_13-bubble-sort ./ch06-arrays/6_14-binary-sort ./ch06-arrays/6_17-enum ./ch06-arrays/6_18-typedef ./ch06-arrays/6_21-multi-array-params ./ch06-arrays/p6_5-print-array-addr-contents
	make ./ch07-pointers/7_4-assign-var-addr-dereference ./ch07-pointers/7_6-pointer-to-pointer ./ch07-pointers/7_8-return-reference ./ch07-pointers/7_10-traverse-array-pointer ./ch07-pointers/7_15-dynamic-arrays-pass-params 
	make ./ch08-c-strings/8_2-assign-strings-null-term ./ch08-c-strings/8_3-cin-terminate-on-new-line ./ch08-c-strings/8_4-cin-getline-terminate-newline ./ch08-c-strings/8_5-cin-getline-terminate-param ./ch08-c-strings/8_8-cin-putback-ignore ./ch08-c-strings/p9_3
	make ./ch10-classes/10_10-copy-constructor ./ch10-classes/10_11-destructor ./ch10-classes/10_13-link-list ./ch10-classes/10_14-static-public-member ./ch10-classes/10_15-static-private-member ./ch10-classes/10_16-static-private-member-static-func-pair
	make ./ch13-templates-iterators/13_2-bubble-sort ./ch13-templates-iterators/13_3-stack-class-template ./ch13-templates-iterators/13_4-vector
	make ./ch14-vectors/14_1-vector-strings ./ch14-vectors/14_2-push_back_size ./ch14-vectors/14_3-vector-iterator ./ch14-vectors/14_7-erase ./ch14-vectors/14_8-insert
clean:
	rm -rf ch01-basics/one ch01-basics/two ch01-basics/three ch01-basics/four ch01-basics/five
	rm -rf ./ch02-types/2_8 ./ch02-types/2_9 ./ch02-types/gcd ./ch02-types/reverse
	rm -rf ./ch05-functions/5_13-prime ./ch05-functions/5_16-pass-by-ref ./ch05-functions/5_20-inline ./ch05-functions/5_22-overloading ./ch05-functions/5_25-default-params-overloading ./ch05-functions/p5_16-format-output ./ch05-functions/p5_17-extract-any-digit ./ch05-functions/p5_22-is-squared
	rm -rf ./ch06-arrays/6_13-bubble-sort ./ch06-arrays/6_14-binary-sort ./ch06-arrays/6_17-enum ./ch06-arrays/6_18-typedef ./ch06-arrays/6_21-multi-array-params ./ch06-arrays/p6_5-print-array-addr-contents
	rm -rf ./ch07-pointers/7_4-assign-var-addr-dereference ./ch07-pointers/7_6-pointer-to-pointer ./ch07-pointers/7_8-return-reference ./ch07-pointers/7_10-traverse-array-pointer ./ch07-pointers/7_15-dynamic-arrays-pass-params 
	rm -rf ./ch08-c-strings/8_2-assign-strings-null-term ./ch08-c-strings/8_3-cin-terminate-on-new-line ./ch08-c-strings/8_4-cin-getline-terminate-newline ./ch08-c-strings/8_5-cin-getline-terminate-param ./ch08-c-strings/8_8-cin-putback-ignore ./ch08-c-strings/p9_3
	rm -rf ./ch10-classes/10_10-copy-constructor ./ch10-classes/10_11-destructor ./ch10-classes/10_13-link-list ./ch10-classes/10_14-static-public-member ./ch10-classes/10_15-static-private-member ./ch10-classes/10_16-static-private-member-static-func-pair
	rm -rf ./ch13-templates-iterators/13_2-bubble-sort ./ch13-templates-iterators/13_3-stack-class-template ./ch13-templates-iterators/13_4-vector
	rm -rf ./ch14-vectors/14_1-vector-strings ./ch14-vectors/14_2-push_back_size ./ch14-vectors/14_3-vector-iterator ./ch14-vectors/14_7-erase ./ch14-vectors/14_8-insert
