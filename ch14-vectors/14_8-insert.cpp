#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
const int SIZE = 8;

typedef std::vector<string> Strings;

void load( Strings &v )
{
    v.push_back("Japan");
    v.push_back("Italy");
    v.push_back("Spain");
    v.push_back("Egypt");
    v.push_back("Chile");
    v.push_back("Zaire");
    v.push_back("Nepal");
    v.push_back("Kenya");
}

void print( Strings v )
{
    for (unsigned long i = 0; i < v.size(); ++i) {
        std::cout << v[i] << std::endl;
    }
    std::cout << std::endl;
}

int main(void)
{
    std::cout << "Just demo how to remove a whole range in vector" << std::endl;
    std::cout << std::endl;
    Strings v;
    load(v);
    sort( v.begin() , v.end() );
    print(v);
    v.erase( v.begin() + 2 , v.end() -2 );
    print(v);
    v.insert( v.begin() + 2 , "India" );
    print(v);
    
    return 0;
}
