#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef std::vector<string> Strings;
typedef Strings::iterator Sit;

void print( Strings v )
{
    for (Sit it = v.begin(); it!=v.end(); ++it) {
        std::cout << *it << std::endl;
    }
}

void load( Strings& v )
{
    v.push_back("Japan");
    v.push_back("Italy");
    v.push_back("Spain");
    v.push_back("Egypt");
    v.push_back("Chile");
    v.push_back("Zaire");
    v.push_back("Nepal");
    v.push_back("Kenya");
}

int main(void)
{
    Strings v;
    load(v);
    print(v);
    
    return 0;
}
