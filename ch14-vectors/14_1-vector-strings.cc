#include <iostream>
#include <string>
#include <vector>
using namespace std;
const int SIZE = 8;

void load( std::vector<string>&v )
{
    v[0] = "Japan";
    v[1] = "Italy";
    v[2] = "Spain";
    v[3] = "Egypt";
    v[4] = "Chile";
    v[5] = "Zaire";
    v[6] = "Nepal";
    v[7] = "Kenya";
}

void print( std::vector<string> v )
{
    for (int i = 0; i < SIZE; ++i) {
        std::cout << v[i] << std::endl;
    }
}

int main(void)
{
    std::vector<string> v(SIZE);
    load(v);
    print(v);
    
    return 0;
}
