#include <iostream>

class Widget
{
public:
    Widget () { ++count; }
    virtual ~Widget () { --count; }
    static int count;
};
int Widget::count = 0;

int main(void)
{
    Widget w , x;
    std::cout << "Now there are " << w.count << " widgets." << std::endl;

    {
        Widget w , x , y , z;
        std::cout << "Now there are " << w.count << " widgets." << std::endl;
    }

    std::cout << "Now there are " << w.count << " widgets." << std::endl;

    Widget y;
    std::cout << "Now there are " << w.count << " widgets." << std::endl;
    
    return 0;
}
