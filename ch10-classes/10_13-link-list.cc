#include <iostream>
using namespace std;

class Node
{
public:
    Node ( int d , Node *q=0 ) : data(d) , next(q) {};
    int data;
    Node *next;
};

int main(void)
{
    int n;
    Node *p , *q = NULL;

    while ( cin >> n ) {
        p = new Node( n , q );
        q = p;
    }
    for( ; p; p = p->next ) {
        std::cout << p->data << " -> ";
    }
    std::cout << "NULL" << std::endl;
    
    return 0;
}
