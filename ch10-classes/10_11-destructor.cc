#include <iostream>

class Ratio
{
public:
    Ratio () { std::cout << "OBJ IS BORN" << std::endl; };
    virtual ~Ratio () { 
        std::cout << "OBJ IS KILLED" << std::endl; 
        std::cout << std::endl;
    };

private:
    int num , den;
};

int main(void)
{
    {
        Ratio x;
        std::cout << "Now x is alive." << std::endl;
    }

    std::cout << "Now between blocks." << std::endl;
    std::cout << std::endl;

    {
        Ratio y;
        std::cout << "Now y is alive." << std::endl;
    }
    
    return 0;
}
