#include <iostream>

class Widget
{
public:
    Widget () { ++count; }
    virtual ~Widget () { --count; }
    static int numWidgets() { return count; }
private:
    static int count;
};
int Widget::count = 0;

int main(void)
{
    Widget w , x;
    // Can also be called like below as in previous example but better to be independent for clarity
    //std::cout << "Now there are " << w.numWidgets() << " widgets." << std::endl;
    std::cout << "Now there are " << Widget::numWidgets() << " widgets." << std::endl;

    {
        Widget w , x , y , z;
        std::cout << "Now there are " << Widget::numWidgets() << " widgets." << std::endl;
    }

    std::cout << "Now there are " << Widget::numWidgets() << " widgets." << std::endl;

    Widget y;
    std::cout << "Now there are " << Widget::numWidgets() << " widgets." << std::endl;
    
    return 0;
}
