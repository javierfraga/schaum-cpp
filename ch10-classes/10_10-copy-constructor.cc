#include <iostream>

class Ratio
{
public:
    Ratio (int n = 0 , int d = 1) : num(n) , den(d) {}
    Ratio( const Ratio &r )       : num(r.num) , den(r.num){
        std::cout << "Copy Constructor Called" << std::endl;
    }

private:
    int num , den;
};

Ratio f( Ratio r ) // 2nd call to copy constructor, copying x to r
{
    Ratio s = r;   // 3rd call to copy constructor, copying r to s
    return s;      // 4th call to copy constructor, copying s to return value
}

int main(void)
{
    Ratio x(22,7);
    Ratio y(x);    // 1st call to copy constructor, copying x to y
    f(y);
    
    return 0;
}
